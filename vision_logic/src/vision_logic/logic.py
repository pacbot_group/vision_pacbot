#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
"""
| author:
| Belal HMEDAN, 
| LIG lab/ Marvin Team, 
| France, 2023.
| VISON Logic.
"""
import rospy
import rospkg
from std_msgs.msg import String, Int16
import sys
import json
import copy
from math import ceil
import numpy as np

# =====================
# Vision Logic Class |
# =====================


class VisionLogic:
    def __init__(self, pattern, arm):
        """
        Class VisionLogic: The Logic used to identify Pick/Place operations. 
        """
        rospack = rospkg.RosPack()
        self.path = rospack.get_path("vision_logic")
        self.pattern = pattern
        self.arm = arm
        self.planning_level = 1
        self.assembly_errors = []
        self.picked = {}
        self.placed = {}
        self.picked_colors = {"r": 0, "b": 0, "y": 0, "w": 0, "l": 0, "o": 0}
        self.old_vision_dict = {}
        self.old_model = {}
        self.vision_dict = {}
        self.model = {}
        self.vision_dict_state = {}
        self.model_state = {}
        self.lego_map = {}
        self.occupied_positions = []
        # Green Cubes Around Assembly Zone
        self.depth_threshold = 4
        self.loadData()
        self.visionReader()

    def loadData(self):
        """
        Function: loadData, to load Legos position/orientation and past state.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        with open(self.path + "/database/vision_dict_state.json", "r") as fp:
            self.vision_dict_state = json.load(fp)[self.pattern][self.arm]

        with open(self.path + "/database/model_state.json", "r") as fp2:
            self.model_state = json.load(fp2)

        with open(self.path + "/database/lego_map.json", "r") as fp3:
            self.lego_map = json.load(fp3)[self.pattern][self.arm]

        for lego in self.lego_map:
            self.depth_threshold += ceil(len(self.lego_map[lego])/2)
        rospy.logwarn("Depth Threshold is: {}".format(self.depth_threshold))

    def key2pos(self, s):
        """
        Function: key2pos, to obtain x, y cell coordinates from string of shape
        'p_xx_yy'. --- Parameters: @param: s, string of shape 'p_xx_yy'. ---
        @return: tuple, (x, y), the cell coordinates.
        """
        o = s.split("_")
        return [int(o[1]), int(o[2])]

    def getBlockByPos(self, pos):
        """
        Function: getBlockByPos, get a list of the legos depending on their p_xx_yy position.
        ---
        Parameters:
        @param: pos, string, p_xx_yy, position.
        ---
        @return: lego_xx_yy, list of strings, all legos that belong to this position p_xx_yy.
        """
        lego_xx_yy = []
        for lego in self.lego_map:
            if pos in self.lego_map[lego]:
                lego_xx_yy.append(lego)
        return lego_xx_yy

    def getMaxZ(self, lego_xx_yy):
        """
        Function: getMaxZ, get the top lego in a list of legos shares same xx_yy position.
        ---
        Parameters:
        @param: lego_xx_yy, list of strings, all legos that belong to this position p_xx_yy.
        ---
        @return: lego_max, max_z, (string, the maximum lego; int, maximum Z).
        """
        max_z = 0
        lego_max = lego_xx_yy[0]
        for lego in lego_xx_yy:
            if (lego[1] == "c") and (self.lego_map[lego][1] > max_z):
                max_z = self.lego_map[lego][1]
                lego_max = lego
            elif (lego[1] == "b") and (self.lego_map[lego][2] > max_z):
                max_z = self.lego_map[lego][2]
                lego_max = lego
        return lego_max, max_z

    def getMaxLevel(self, pos_dict):
        """
        Function getMaxLevel, to get the max level in the model.
        ---
        Paramrters:
        @param: pos_dict, dictionary, eg: {'1':'w', '3': 'r'}
        ---
        @return: max_level, int, eg: 3, or 0.
        """
        if (bool(pos_dict)):
            levels = [eval(i) for i in list(pos_dict.keys())]
            max_level = np.max(np.array(levels))
        else:
            max_level = 0
        return int(max_level)

    def neighbours(self, pos):
        """
        Function: neighbours, to find the nighbouring position, and the rotation for a lego Brick.
        ---
        Parameters:
        @param: pos, string, p_xx_yy, position.
        ---
        @return: None
        """
        x0, y0 = self.key2pos(pos)

        p1 = "p_" + f"{x0+1:02}" + "_" + f"{y0:02}"
        p2 = "p_" + f"{x0-1:02}" + "_" + f"{y0:02}"
        p3 = "p_" + f"{x0:02}" + "_" + f"{y0+1:02}"
        p4 = "p_" + f"{x0:02}" + "_" + f"{y0-1:02}"

        if p1 in self.placed:
            if self.placed[p1] == self.placed[pos]:
                return [pos, p1, True]
        if p2 in self.placed:
            if self.placed[p2] == self.placed[pos]:
                return [p2, pos, True]
        if p3 in self.placed:
            if self.placed[p3] == self.placed[pos]:
                return [pos, p3, False]
        if p4 in self.placed:
            if self.placed[p4] == self.placed[pos]:
                return [p4, pos, False]
        if (pos in self.old_model):
            if self.getBlockByPos(p1):
                z_1 = self.getMaxLevel(self.old_model[p1])
                if (z_1 == 0):
                    c_1 = 'g'
                else:
                    # TODO: check if model keys are strings or integers!
                    c_1 = self.old_model[p1][str(z_1)]
                lego, _ = self.getMaxZ(self.getBlockByPos(p1))
                if (lego[1] == "b") and ([c_1, z_1] == self.placed[pos]):
                    return [pos, p1, True]
            if self.getBlockByPos(p2):
                z_2 = self.getMaxLevel(self.old_model[p2])
                if (z_2 == 0):
                    c_2 = 'g'
                else:
                    # TODO: check if model keys are strings or integers!
                    c_2 = self.old_model[p2][str(z_2)]
                lego, _ = self.getMaxZ(self.getBlockByPos(p2))
                if (lego[1] == "b") and ([c_2, z_2] == self.placed[pos]):
                    return [p2, pos, True]
            if self.getBlockByPos(p3):
                z_3 = self.getMaxLevel(self.old_model[p3])
                if (z_3 == 0):
                    c_3 = 'g'
                else:
                    # TODO: check if model keys are strings or integers!
                    c_3 = self.old_model[p3][str(z_3)]
                lego, _ = self.getMaxZ(self.getBlockByPos(p3))
                if (lego[1] == "b") and ([c_3, z_3] == self.placed[pos]):
                    return [pos, p3, False]
            if self.getBlockByPos(p4):
                z_4 = self.getMaxLevel(self.old_model[p4])
                if (z_4 == 0):
                    c_4 = 'g'
                else:
                    # TODO: check if model keys are strings or integers!
                    c_4 = self.old_model[p4][str(z_4)]
                lego, _ = self.getMaxZ(self.getBlockByPos(p4))
                if (lego[1] == "b") and ([c_4, z_4] == self.placed[pos]):
                    return [p4, pos, False]
        else:
            if self.getBlockByPos(p1):
                lego, _ = self.getMaxZ(self.getBlockByPos(p1))
                if (lego[1] == "b") and (self.old_vision_dict[p1] == self.placed[pos]):
                    return [pos, p1, True]
            if self.getBlockByPos(p2):
                lego, _ = self.getMaxZ(self.getBlockByPos(p2))
                if (lego[1] == "b") and (self.old_vision_dict[p2] == self.placed[pos]):
                    return [p2, pos, True]
            if self.getBlockByPos(p3):
                lego, _ = self.getMaxZ(self.getBlockByPos(p3))
                if (lego[1] == "b") and (self.old_vision_dict[p3] == self.placed[pos]):
                    return [pos, p3, False]
            if self.getBlockByPos(p4):
                lego, _ = self.getMaxZ(self.getBlockByPos(p4))
                if (lego[1] == "b") and (self.old_vision_dict[p4] == self.placed[pos]):
                    return [p4, pos, False]
        return False

    def updateLegoMap(self, lego, pos, rot=False):
        """
        Function: updateLegoMap, to update the lego_map (maps legos to positions).
        ---
        Parameters:
        @param: lego, string, i.e: yb1, y --> yellow color, b/c --> brick/cube
        @param: pos, string/List of two strings cube/brick, p_xx_yy, [p_xx1_yy1, p_xx2_yy2] positions.
        @param: depth, int, to represent the placement depth of the lego.
        @param: rot, rotation of the lego brick.
        ---
        @return: None
        """
        # TODO: Check whether vision_dict or model has to be used here.

        rospy.logwarn("Update Lego: ({}), pos: {}".format(lego, pos))
        # cube
        if lego[1] == "c":
            if (pos in self.old_model):
                if (self.getMaxLevel(self.old_model[pos]) != 0):
                    self.lego_map[lego] = [
                        pos, self.getMaxLevel(self.old_model[pos])]
                else:
                    rospy.logerr("updateLegoMap Empty Cube Pos ({}) in Model: {}".format(
                        pos, self.old_model))
            else:
                self.lego_map[lego] = [pos, self.old_vision_dict[pos][1]]
        # Brick
        elif lego[1] == "b":
            if (pos[0] in self.old_model and pos[1] in self.old_model):
                if (self.getMaxLevel(self.old_model[pos[0]]) != 0 and self.getMaxLevel(self.old_model[pos[1]]) != 0):
                    self.lego_map[lego] = [
                        pos[0], pos[1],
                        self.getMaxLevel(self.old_model[pos[0]]),
                        rot]
                else:
                    rospy.logerr("updateLegoMap Empty Block Pos ({}) in Model: {}".format(
                        pos, self.old_model))
            else:
                self.lego_map[lego] = [
                    pos[0], pos[1],
                    self.old_vision_dict[pos[0]][1],
                    rot]

    def fillOccupiedPositions(self):
        """
        Function: fillOccupiedPositions, to construct a list of the positions occupied by Legos.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        self.occupied_positions = []

        for lego in self.lego_map:
            if lego[1] == "c":
                pos = self.lego_map[lego][0]
                if not (pos in self.occupied_positions):
                    self.occupied_positions.append(pos)
            else:
                p1 = self.lego_map[lego][0]
                p2 = self.lego_map[lego][1]
                if not (p1 in self.occupied_positions):
                    self.occupied_positions.append(p1)
                if not (p2 in self.occupied_positions):
                    self.occupied_positions.append(p2)

    def pickPlaceMatcher(self):
        """
        Function: pickPlaceMatcher, to match the legos (same color/size) in the pick/place dictionaries.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        self.placed_neighbours = []
        self.popped_lego_bricks = []
        # Loop the Bricks First
        for lego in list(self.picked):
            for place_pos in list(self.placed):
                if ((place_pos in self.placed_neighbours) or (lego in self.popped_lego_bricks)):
                    rospy.logwarn("Lego ({}) Was NOT Placed due to PN {} or Popped {}".format(
                        lego, place_pos in self.placed_neighbours, lego in self.popped_lego_bricks))
                    continue
                # Brick Same Color is Placed with neighbour
                if (
                    (lego[0] == self.placed[place_pos][0])
                    and (lego[1] == "b")  # Brick
                    and (self.neighbours(place_pos))
                ):
                    pos1, pos2, rot = self.neighbours(place_pos)
                    self.placed_neighbours.append(pos1)
                    self.placed_neighbours.append(pos2)
                    self.updateLegoMap(lego, [pos1, pos2], rot)
                    ppick1, ppick2 = self.picked[lego][0:2]
                    self.picked.pop(lego)
                    self.popped_lego_bricks.append(lego)
                    self.picked_colors[lego[0]
                                       ] = self.picked_colors[lego[0]] - 1

                    rospy.logwarn("pickPlaceMatcher: Lego Brick ({}) was picked and placed in {}, {}".format(
                        lego, pos1, pos2))

                    if pos1 in self.placed:
                        self.placed.pop(pos1)
                    if pos2 in self.placed:
                        self.placed.pop(pos2)

                    if (ppick1 in self.old_model and ppick2 in self.model):
                        self.model_state[ppick1] = self.old_model[ppick1]
                        self.model_state[ppick2] = self.old_model[ppick2]
                    else:
                        self.vision_dict_state[ppick1] = self.old_vision_dict[ppick1]
                        self.vision_dict_state[ppick2] = self.old_vision_dict[ppick2]
                    if (pos1 in self.old_model and pos2 in self.model):
                        self.model_state[pos1] = self.old_model[pos1]
                        self.model_state[pos2] = self.old_model[pos2]
                    else:
                        self.vision_dict_state[pos1] = self.old_vision_dict[pos1]
                        self.vision_dict_state[pos2] = self.old_vision_dict[pos2]

        self.popped_lego_cubes = []
        # Loop the Cubes After
        for lego in list(self.picked):
            for place_pos in list(self.placed):
                if lego in self.popped_lego_cubes:
                    continue
                # Cube Same Color
                if (lego[0] == self.placed[place_pos][0]) and (lego[1] == "c"):
                    self.updateLegoMap(lego, place_pos)
                    ppick = self.picked[lego][0]
                    self.popped_lego_cubes.append(lego)
                    self.picked.pop(lego)
                    self.placed.pop(place_pos)
                    self.picked_colors[lego[0]
                                       ] = self.picked_colors[lego[0]] - 1

                    rospy.logwarn("pickPlaceMatcher: Lego Cube ({}) was picked and placed in {}".format(
                        lego, place_pos))
                    if (ppick in self.old_model):
                        self.model_state[ppick] = self.old_model[ppick]
                    else:
                        self.vision_dict_state[ppick] = self.old_vision_dict[ppick]
                    if (place_pos in self.old_model):
                        self.model_state[place_pos] = self.old_model[place_pos]
                    else:
                        self.vision_dict_state[place_pos] = self.old_vision_dict[place_pos]

        if (not bool(self.placed)):
            self.placed = {}
            rospy.logwarn("pickPlaceMatcher: placed legos cleared!")

    def pick(self):
        """
        Function: pick, to identify which legos are picked from the change in the color/depth information.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        for lego in self.lego_map:
            # Color
            c = lego[0]
            # Brick
            if (lego[1] == 'b'):
                p1_ = self.lego_map[lego][0]
                p2_ = self.lego_map[lego][1]
                z = self.lego_map[lego][2]

                # Assembly Zone
                if ((p1_ in self.old_model and p2_ in self.old_model) and ((z >= self.planning_level) or p1_ in self.assembly_errors or p2_ in self.assembly_errors)):
                    # if((p1_ in self.old_model and p2_ in self.old_model) and (p1_ in self.assembly_errors or p2_ in self.assembly_errors)):

                    if (
                        self.old_model[p1_] != self.model_state[p1_]
                        or self.old_model[p2_] != self.model_state[p2_]
                    ):
                        rospy.logwarn('Pick: model: p1 {} old state is {}, and new state is {}'.format(
                            p1_, self.model_state[p1_], self.old_model[p1_]))
                        rospy.logwarn('Pick: model: p2 {} old state is {}, and new state is {}'.format(
                            p2_, self.model_state[p2_], self.old_model[p2_]))
                        z_old1 = self.getMaxLevel(self.model_state[p1_])
                        z_old2 = self.getMaxLevel(self.model_state[p2_])
                        z_new1 = self.getMaxLevel(self.old_model[p1_])
                        z_new2 = self.getMaxLevel(self.old_model[p2_])
                        if ((z == z_old1 and z == z_old2) and (z_new1 < z_old1 or z_new2 < z_old2) or (z_new1 == z_old1 and z_new2 == z_old2)):
                            rospy.logwarn("Pick: model: lego ({}) was picked from {}-{}, \ncurrent state: {}-{}\npast state: {}-{}".format(
                                lego, p1_, p2_, self.old_model[p1_], self.old_model[p2_], self.model_state[p1_], self.model_state[p2_]))
                            # TODO: Check if there is a rotated lego bricks to pick.
                            self.picked[lego] = [p1_, p2_, z_old1]
                            self.picked_colors[c] = self.picked_colors[c] + 1
                            rospy.logwarn(
                                "Pick: model: Lego Brick ({}) was Picked".format(lego))
                # Outside Assembly Zone
                elif (p1_ in self.old_vision_dict and p2_ in self.old_vision_dict):
                    if (
                        self.old_vision_dict[p1_] != self.vision_dict_state[p1_]
                        or self.old_vision_dict[p2_] != self.vision_dict_state[p2_]
                    ):
                        rospy.logwarn('Pick: vd: p1 {} old state is {}, and new state is {}'.format(
                            p1_, self.vision_dict_state[p1_], self.old_vision_dict[p1_]))
                        rospy.logwarn('Pick: vd: p2 {} old state is {}, and new state is {}'.format(
                            p2_, self.vision_dict_state[p2_], self.old_vision_dict[p2_]))
                        z_old_1 = int(self.vision_dict_state[p1_][1])
                        z_old_2 = int(self.vision_dict_state[p2_][1])
                        z_new_1 = int(self.old_vision_dict[p1_][1])
                        z_new_2 = int(self.old_vision_dict[p2_][1])
                        if ((z == z_old_1 and z == z_old_2) and (z_new_1 < z_old_1 or z_new_2 < z_old_2) or (z_new_1 == z_old_1 and z_new_2 == z_old_2)):
                            rospy.logwarn("Pick: vd: lego ({}) was picked from {}-{}, \ncurrent state: {}-{}\npast state: {}-{}".format(
                                lego, p1_, p2_, self.old_vision_dict[p1_], self.old_vision_dict[p2_], self.vision_dict_state[p1_], self.vision_dict_state[p2_]))
                            # TODO: Check if there is a rotated lego bricks to pick.
                            self.picked[lego] = [
                                p1_, p2_, self.vision_dict_state[p1_]]
                            self.picked_colors[c] = self.picked_colors[c] + 1
                            rospy.logwarn(
                                "Pick: vd: Lego Brick ({}) was Picked in the Loop".format(lego))
                else:
                    rospy.logerr(
                        "Pick: Lego Brick Positions {}-{} is Unknown!".format(p1_, p2_))
            # Cube
            elif (lego[1] == 'c'):
                p_ = self.lego_map[lego][0]
                z = self.lego_map[lego][1]

                # Assembly Zone
                if (p_ in self.old_model and ((z >= self.planning_level) or p_ in self.assembly_errors)):
                    z_old = self.getMaxLevel(self.model_state[p_])
                    z_new = self.getMaxLevel(self.old_model[p_])

                    # information change (color/depth) at this position
                    if (self.old_model[p_] != self.model_state[p_]):
                        if ((z == z_old) and (z_new <= z_old)):
                            if (z_old != 0):
                                self.picked[lego] = [p_, z_new]
                                self.picked_colors[c] = self.picked_colors[c] + 1
                                rospy.logwarn(
                                    "Pick: Lego Cube ({}) was Picked from {} Model".format(lego, p_))
                            else:
                                rospy.logerr("Pick: Lego Cube ({}) in pos: {} is empty model {}".format(
                                    lego, p_, self.model_state))
                # Outside Assembly Zone
                elif (p_ in self.old_vision_dict):
                    # information change (color/depth) at this position
                    if self.old_vision_dict[p_] != self.vision_dict_state[p_]:
                        if self.old_vision_dict[p_][1] <= self.vision_dict_state[p_][1]:
                            self.picked[lego] = [
                                p_, self.vision_dict_state[p_]]
                            self.picked_colors[c] = self.picked_colors[c] + 1
                            rospy.logwarn(
                                "Pick: Lego Cube ({}) was Picked from {} Vision_Dict".format(lego, p_))
                else:
                    rospy.logerr(
                        "Pick: Lego Cube Position {} is Unknown!".format(p_))

    def place(self):
        """
        Function: place, to identify which legos are placed from the change in the color/depth information.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        for pos in self.old_vision_dict:
            # Assembly Zone
            if (pos in self.old_model):
                z_old = self.getMaxLevel(self.model_state[pos])
                z_new = self.getMaxLevel(self.old_model[pos])
                if (z_new != 0):
                    # TODO: check if model keys are strings or integers!
                    c_new = self.old_model[pos][str(z_new)]
                    depth_diff = z_new - z_old
                else:
                    c_new = 'g'
                if self.old_model[pos] != self.model_state[pos]:

                    if ((depth_diff == 1) or (depth_diff == 0)) and (c_new != 'g'):
                        if (self.picked_colors[c_new] > 0):
                            self.placed[pos] = [c_new, z_new]

            # Outside Assembly Zone
            else:
                depth_diff = int(
                    self.old_vision_dict[pos][1]) - int(self.vision_dict_state[pos][1])
                if self.old_vision_dict[pos] != self.vision_dict_state[pos]:
                    if ((depth_diff == 1) or (depth_diff == 0)) and (self.old_vision_dict[pos][0] != 'g'):
                        if (self.picked_colors[self.old_vision_dict[pos][0]] > 0):
                            self.placed[pos] = self.old_vision_dict[pos]

    def pickPlaceFinder(self):
        """
        Function: pickPlaceFinder, to find any change in the vision information in order to identify whether it is a pick or place.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        # self.new_vision_dict = False
        # self.new_model = False

        rospy.logwarn("pickPlaceFinder Started, picked/placed cleared!")
        self.picked = {}
        self.placed = {}
        self.pick()
        self.place()

        rospy.logwarn("\n pickPlaceFinder \n")
        rospy.logwarn("\n--------Picked--------\n")
        rospy.logwarn(str(self.picked))
        rospy.logwarn("\n--------Placed--------\n")
        rospy.logwarn(str(self.placed))
        rospy.logwarn("\n======================\n")
        # rospy.logwarn("pickPlaceFinder: before matching: Lego Map: {}".format(self.lego_map))
        self.pickPlaceMatcher()
        # rospy.logwarn("pickPlaceFinder: after matching: Lego Map: {}".format(self.lego_map))
        self.fillOccupiedPositions()

    def visionUpdate(self, data):
        """
        Function: visionUpdate, subscriber callback to find any change in the vision information in order to identify whether it is a pick or place operation.
        ---
        Parameters:
        @param: data, string, JSON string containing Vision Dictionary.
        ---
        @return: None
        """
        self.vision_dict = json.loads(data.data)
        if (self.vision_dict != self.old_vision_dict):
            self.depth_counter = 0
            # Correct Depth from Color
            for p_ in self.vision_dict:
                if (int(self.vision_dict[p_][1]) == 0) and (self.vision_dict[p_][0] != "g"):
                    self.vision_dict[p_][1] = 1
                    # # Correct Model from Color
                    # if(p_ in self.old_model):
                    #     self.old_model[p_].update({'1': self.vision_dict[p_][0]})

                    rospy.logwarn("Position ({}) Depth Correction from Color {}".format(
                        p_, self.vision_dict[p_][0]))
                self.depth_counter += int(self.vision_dict[p_][1])

            rospy.logwarn("Depth Counter: ({}), Depth Threshold: ({})".format(
                self.depth_counter, self.depth_threshold))
            self.old_vision_dict.update(self.vision_dict)
            # self.new_vision_dict = True
            rospy.logwarn("Vision Update: New Vision Dict Received")
            # TODO: depth threshold equality conditions
            # means that the vision will be blocked if any block was picked!
            # better to find another condition.
            if (self.depth_counter == self.depth_threshold):
                self.pickPlaceFinder()
        self.publishLegoMap()

    def modelUpdate(self, data):
        """
        Function: modelUpdate, subscriber callback to the vision model.
        ---
        Parameters:
        @param: data, string, JSON string containing Vision Model.
        ---
        @return: None
        """
        self.model = json.loads(data.data)
        if (self.model != self.old_model):
            self.old_model.update(self.model)
            # self.new_model = True
            rospy.logwarn("Model Update: New Model Received")
            # TODO: depth threshold equality conditions
            # means that the vision will be blocked if any block was picked!
            # better to find another condition.
            # if(self.depth_counter == self.depth_threshold):
            #     self.pickPlaceFinder()
        # self.publishLegoMap()

    def publishLegoMap(self):
        """
        Function: publishLegoMap, ROS publisher to publish Lego Map.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        msg_to_pub = copy.deepcopy(self.lego_map)
        # rospy.logwarn("Publish Lego Map: msg to pub: {}, picked: {}".format(msg_to_pub, self.picked))
        for lego in self.picked:
            rospy.logwarn(
                "pickPlaceFinder: Lego ( {} ) was picked, but not placed!".format(
                    lego)
            )
            del msg_to_pub[lego]
        # rospy.logwarn("Publish Lego Map: msg to pub After: {} ".format(type(msg_to_pub)))
        lego_map = json.dumps(msg_to_pub)
        self.lego_map_pub.publish(lego_map)

    def levelUpdate(self, data):
        """
        Function: levelUpdate, subscriber callback to the max assembly level.
        ---
        Parameters:
        @param: data, int, max assembly level.
        ---
        @return: None
        """
        self.planning_level = data.data

    def errorsUpdate(self, data):
        """
        Function: errorsUpdate, subscriber callback to the errors in the assembly.
        ---
        Parameters:
        @param: data, string, assembly errors dictionary.
        ---
        @return: None
        """
        self.errors = json.loads(data.data)

    def visionReader(self):
        """
        Function: visionReader, to read the discrete vision information.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        rospy.init_node("vision_rational", anonymous=True)
        self.lego_map_pub = rospy.Publisher("lego_map", String, queue_size=10)
        rospy.Subscriber("vision_dict", String, self.visionUpdate)
        rospy.Subscriber("model", String, self.modelUpdate)
        rospy.Subscriber("planning_level", Int16, self.levelUpdate)
        rospy.Subscriber("assembly_errors", String, self.errorsUpdate)
        rospy.sleep(1)
        rospy.spin()


# =============================================
if __name__ == "__main__":
    try:
        vl_ = VisionLogic(sys.argv[1], sys.argv[2])
    except rospy.ROSInterruptException:
        rospy.logerr("Error in the Vision Logic")

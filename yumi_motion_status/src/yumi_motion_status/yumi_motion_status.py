#!/usr/bin/env python3.8
# -*- coding: utf-8-*-
"""
| author:
| Belal HMEDAN, 
| LIG lab/ Marvin Team, 
| France, 2023.
| YuMi motion status node publisher.
"""

import rospy
from std_msgs.msg import Bool
from sensor_msgs.msg import JointState
import numpy as np

class YuMiMotion():
    def __init__(self) -> None:
        """
        
        """
        self.motion_status = False
        
        self.left_calib_joints  = np.array([0.0, -2.271, 2.352, 0.523, 0.0, 0.7, 0.0])
        self.right_calib_joints = np.array([0.0, -2.267, -2.358, 0.525, 0.0, 0.7, 0.0])
        
        self.left_pickap_joints  = np.array([-0.3, -1.037, 1.0123, 0.152, 1.068, 0.859, 3.495])
        self.right_pickap_joints = np.array([0.251, -1.66, -1.002, 0.376, -1.064, 1.082, -0.629])

        self.atol = 1e-02
        self.rtol = 1e-02

        rospy.init_node("motion_status", anonymous=True)
        self.motion_pub = rospy.Publisher("yumi_motion_status", Bool, queue_size=10)
        self.jointStateSubscriber()
    
    def motionCB(self, data):
        """
        
        """
        motion_left = False
        motion_right = False

        joint_values = data.position

        right = np.array([round(joint_values[0], 3), round(joint_values[1], 3), round(joint_values[2], 3), round(joint_values[3], 3), round(joint_values[4], 3), round(joint_values[5], 3), round(joint_values[6], 3)])
        left  = np.array([round(joint_values[7], 3), round(joint_values[8], 3), round(joint_values[9], 3), round(joint_values[10], 3), round(joint_values[11], 3), round(joint_values[12], 3), round(joint_values[13], 3)]) 

        if(np.allclose(right, self.right_calib_joints, rtol=self.rtol, atol=self.atol) or np.allclose(right, self.right_pickap_joints, rtol=self.rtol, atol=self.atol)):
            motion_right = False
        else:
            rospy.logwarn("right arm is moving")
            motion_right = True

        if(np.allclose(left, self.left_calib_joints, rtol=self.rtol, atol=self.atol) or np.allclose(left, self.left_pickap_joints, rtol=self.rtol, atol=self.atol)):
            motion_left = False
        else:
            rospy.logwarn("left arm is moving")
            motion_left = True

        self.motion_status = motion_left or motion_right
        self.motion_pub.publish(self.motion_status)
        
    def jointStateSubscriber(self):
        """
        
        """
        self.motion_pub = rospy.Publisher("yumi_motion_status", Bool, queue_size=10)
        self.rate = rospy.Rate(10) # 10hz
        rospy.Subscriber('/yumi/rws/joint_states', JointState, self.motionCB)
        rospy.spin()

##==================================
if __name__ == '__main__':
    yumi_motion_ = YuMiMotion()
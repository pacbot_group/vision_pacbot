#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-
'''
| author:
| Belal HMEDAN,
| LIG lab/ Marvin Team,
| France, 2022.
| Vision Model Validation.
'''
import sys
import rospy
from std_msgs.msg import String, Bool, Int16
import json
import numpy as np
import rospkg
#==========================
# class visionInterpreter |
#==========================

class VisionInterpreter(object):

    def __init__(self, pattern_id):
        """
        Class Vision Interpreter, to interpret the vision data into useful information.
        ---
        Parameters:
        @param: pattern_id, string, "simple" or "complex".
        """
        self.pattern_id = pattern_id
        self.old_model = ""
        self.errors = []
        self.solved = False
        self.model = None
        self.planning_level = 1
        rospack = rospkg.RosPack()
        self.path = rospack.get_path('model_validator')
        self.loadData()
    
    def loadData(self):
        """
        Function: loadData, to load GroundTruth and Solution.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        with open(self.path + "/database/ground_truth.json", "r") as fp:
            self.assembly_gt = json.load(fp)[self.pattern_id]
        with open(self.path + "/database/solution.json", "r") as fp2:
            self.solution = json.load(fp2)[self.pattern_id]

    def getMaxLevel(self, pos_dict):
        """
        Function getMaxLevel, to get the max level in the model.
        ---
        Paramrters:
        @param: pos_dict, dictionary, eg: {'1':'w', '3': 'r'}
        ---
        @return: max_level, dictionary, eg: {'3':'r'}, or {}.
        """
        max_level = {}
        if(bool(pos_dict)):
            levels = [eval(i) for i in list(pos_dict.keys())]
            max_lev_num = np.max(np.array(levels))
            max_level[str(max_lev_num)] = pos_dict[str(max_lev_num)]
        return max_level

    def compareWorld(self):
        """
        Function compareWorld, to check the assembly progress/errors.
        ---
        Paramrters:
        @param: None
        ---
        @return: None
        """
        self.errors = []
        self.solved = True

        for pos in self.assembly_gt:
            # rospy.logwarn('pos {}, in model: {}, in GT: {}'.format(pos, self.model[pos], self.assembly_gt[pos]))

            if(not bool(self.assembly_gt[pos])):
                if(not bool(self.model[pos])):
                    continue
                else:
                    rospy.logwarn("Position {} is {} but it should be Empty!".format(pos, self.model[pos]))
                    self.errors.append(pos)
            else:
                max_level_dict = self.getMaxLevel(self.model[pos])
                if(bool(max_level_dict)):
                    for z_str, color_str in max_level_dict.items():
                        if(not z_str in self.assembly_gt[pos]):
                            rospy.logwarn("Wrong Depth in pos {} is {} but it should be: {}".format(pos, self.model[pos], self.assembly_gt[pos]))
                            self.errors.append(pos)
                        else:
                            if(color_str != self.assembly_gt[pos][z_str]):
                                rospy.logwarn("Wrong Color in pos {} is {} but it should be: {}".format(pos, self.model[pos], self.assembly_gt[pos]))
                                self.errors.append(pos)

        for pos_ in self.solution:
            if(self.solution[pos_]!=self.getMaxLevel(self.model[pos_])):
                rospy.logwarn("Solution pos {} solution is {}, but max_level is {}".format(pos_, self.solution[pos_], self.getMaxLevel(self.model[pos_])))
                self.solved = False

    def checkLevel(self):
        """ """
        max_lev_ = 1
        for pos_ in self.model:
            if(bool(self.model[pos_])):
                max_pos = int(list(self.getMaxLevel(self.model[pos_]).keys())[0])
                if(max_pos>max_lev_): max_lev_= max_pos
        if(self.planning_level>max_lev_): self.planning_level -=1
        #
        level_completed = True
        lev_ = str(self.planning_level)
        #
        for pos in self.assembly_gt:
            if(lev_ in self.assembly_gt[pos]):
                if(lev_ in self.model[pos]):
                    if(self.assembly_gt[pos][lev_]!=self.model[pos][lev_]):
                        level_completed = False                       
                else:
                    level_completed = False
        # Max 5 Levels
        if(level_completed and self.planning_level<5):
            self.planning_level += 1

    def modelValidator(self, data):
        """
        Function modelValidator, to validate the assembly progress from vision information.
        ---
        Paramrters:
        @param: data, String, discrete vision representation.
        ---
        @return: None
        """
        self.model = json.loads(data.data)
        # print(self.errors)
        if(self.model != self.old_model):
            self.old_model = self.model
            self.compareWorld()
            self.checkLevel()
            
        errors_string = json.dumps(self.errors)
        self.errors_pub.publish(errors_string)
        self.solved_pub.publish(self.solved)
        self.level_pub.publish(self.planning_level)

    def receiver(self):
        """
        Function: receiver, to initialize ROS subscriber node to get the vision information.
        ---
        Paramters:
        @param: None
        ---
        @return None
        """
        rospy.init_node('model_validator', anonymous=True)

        self.errors_pub = rospy.Publisher("assembly_errors", String, queue_size=10)
        self.solved_pub = rospy.Publisher("problem_solved", Bool, queue_size=1)
        self.level_pub = rospy.Publisher("planning_level", Int16, queue_size=1)

        rospy.Subscriber('model', String, self.modelValidator)
        rospy.spin()
##===============================================================================

if __name__ == '__main__':
    vision_gui_iface_ = VisionInterpreter(sys.argv[1])
    vision_gui_iface_.receiver()

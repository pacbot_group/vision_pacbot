#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-
'''
| author:
| Belal HMEDAN,
| LIG lab/ Marvin Team,
| France, 2022.
| Vision Model Accumulator.
'''
import rospy
from std_msgs.msg import String
import json
import rospkg
#==========================
# class ModelAcumulator   |
#==========================

class ModelAcumulator:

    def __init__(self):
        """
        Class ModelAcumulator, to accumulate the vision data of all levels.
        """
        rospack = rospkg.RosPack()
        self.path = rospack.get_path("model_accumulator")
        self.model = {}
        self.latcher = 0
        self.extracted_model = {}
        self.loadData()
        self.receiver()

    def loadData(self):
        """
        Function: loadData, to load the model from JSON.
        ---
        Parameters:
        @param: None
        ---
        @return: None
        """
        with open(self.path+'/database/extracted_model.json', 'r') as fp:
            self.extracted_model = json.load(fp)

    def max_non_green(self, dict):
        """
        Function: max_non_green, to return the max non-green level (int).
        """
        for i in range(5, 0, -1):
            if(dict[str(i)]!="g"):
                return str(i)
        return 0

    def copy_model(self, pos, level):
        """
        Function: copy_model, to store the status of model in specific position.
        """
        leve = self.max_non_green(self.model[pos])
        if(leve != 0 and self.model[pos][level]!="g"):
            self.extracted_model[pos][level] = self.model[pos][level]
        elif(leve == 0):
            self.extracted_model[pos] = {}
        elif(self.model[pos][leve]=="g" and leve in self.extracted_model[pos]):
            self.extracted_model[pos].pop(leve)
        ## Make Positions on Top of the Max Green
        for lev in range(int(leve)+1, 6):
            if(str(lev) in self.extracted_model[pos]):
                self.extracted_model[pos].pop(str(lev))

    def extract_model(self):
        """
        Function extract_model, to extract the existent blocks only.
        ---
        Paramrters:
        @param: None
        ---
        @return: None
        """
        for pos in self.model:
            for level in self.model[pos]:
                ## If the level exists in the extracted model
                if (level in self.extracted_model[pos]):
                    if(self.extracted_model[pos][level]!=self.model[pos][level] and self.model[pos][level]!="g"):
                        if self.latcher >= 6:
                            self.copy_model(pos, level)
                            self.latcher = 0
                        else:
                            self.latcher += 2
                else:
                    self.copy_model(pos, level)
        self.latcher -= 1
        
    def model_accumulator(self, data):
        """
        Function model_accumulator, to accumulate vision info into a model.
        ---
        Paramrters:
        @param: data, String, discrete vision representation.
        ---
        @return: None
        """
        ## Read Vision Data
        self.vision_dict = json.loads(data.data)
        for p_ in self.vision_dict:
                if (int(self.vision_dict[p_][1]) == 0) and (self.vision_dict[p_][0] != "g"):
                    self.vision_dict[p_][1] = 1
        ## Reset Model
        for pos_ in self.extracted_model:
            self.model[pos_] = {"1" : "g", "2" : "g", "3" : "g", "4" : "g", "5": "g"}
        ## Fill model
        for pos in self.model:
            self.model[pos][str(self.vision_dict[pos][1])] = self.vision_dict[pos][0]
        self.extract_model()
        # self.dumpData()
        fin_model = json.dumps(self.extracted_model)
        self.model_pub.publish(fin_model)

    def receiver(self):
        """
        Function: receiver, to initialize ROS subscriber node to get the vision information.
        ---
        Paramters:
        @param: None
        ---
        @return None
        """
        rospy.init_node('model_accumulator', anonymous=True)
        self.model_pub = rospy.Publisher("model", String, queue_size=10)
        rospy.Subscriber('vision_dict', String, self.model_accumulator)
        rospy.sleep(1)
        rospy.spin()
##===============================================================================

if __name__ == '__main__':
    vision_iface_ = ModelAcumulator()